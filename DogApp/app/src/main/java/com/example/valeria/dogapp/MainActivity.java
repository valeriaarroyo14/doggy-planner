package com.example.valeria.dogapp;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_camera = (Button)findViewById(R.id.button);
        button_camera.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(),CameraActivity.class);
                startActivity(intent);
            }
        });

        Button button_calendary = (Button)findViewById(R.id.button2);
        button_calendary.setOnClickListener(new View.OnClickListener(){
            public void onClick (View v){
                Intent intent2 =  new Intent(getApplicationContext(),CalendaryActivity.class);
                startActivity(intent2);
            }
        });

        Button button_calendaryapp =  (Button)findViewById(R.id.button3);
        button_calendaryapp.setOnClickListener(new View.OnClickListener(){
            public  void onClick(View v){
                Intent intent3 = new Intent(getApplicationContext(), CalendaryAppActivity.class);
                startActivity(intent3);
            }
        });
    }
}




